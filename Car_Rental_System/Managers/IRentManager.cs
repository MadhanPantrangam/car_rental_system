﻿using Car_Rental_System.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Car_Rental_System.Managers
{
    public interface IRentManager
    {
        List<RentailViewModel> GetRentDetails();
        List<car_Registration> GetCarRegistrations();
        string GetCustomerName(int Id);
        string GetAvailableCar(string carNo);
        string SaveRentDetails();
        car_Registration checkExistRentDetail(string carno);
    }
}
