﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Car_Rental_System.Models;

namespace Car_Rental_System.Managers
{
    public class RentManager : IRentManager
    {
        private supercarEntities1 db;
        public RentManager()
        {
            db = new supercarEntities1();
        }

        public car_Registration checkExistRentDetail(string carno)
        {
            var rent = db.car_Registration.FirstOrDefault(e => e.carno == carno);
            return rent;
        }

        public string GetAvailableCar(string carNo)
        {
            var caravil = (from s in db.car_Registration where s.carno == carNo select s.available).FirstOrDefault();

            return caravil;
        }

        public List<car_Registration> GetCarRegistrations()
        {
            return db.car_Registration.ToList();
        }

        public string GetCustomerName(int Id)
        {
            var customerName = (from s in db.customers where s.id == Id select s.custname).FirstOrDefault();
            return customerName;
        }

        public List<RentailViewModel> GetRentDetails()
        {
            var result = (from r in db.rentails
                          join c in db.car_Registration on r.carid equals c.carno
                          select new RentailViewModel
                          {
                              id = r.id,
                              carid = r.carid,
                              custid = r.custid,
                              fee = r.fee,
                              sdate = r.sdate,
                              edate = r.edate,
                              available = c.available

                          }).ToList();
            return result;
        }

        public string SaveRentDetails()
        {
            throw new NotImplementedException();
        }
    }
}