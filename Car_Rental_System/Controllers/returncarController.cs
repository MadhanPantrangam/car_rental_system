﻿using Car_Rental_System.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Car_Rental_System.Controllers
{


    public class returncarController : Controller
    {

        supercarEntities1 db = new supercarEntities1();

        // GET: returncar
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Save(returncar recar)
        {
            //if( )
            //{
                db.returncars.Add(recar);

                var car = db.car_Registration.SingleOrDefault(e => e.carno == recar.carno);

                if(car == null)
                {
                    return HttpNotFound("CarNo Not Found");
                }
                car.available = "Yes";
                db.Entry(car).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            //}
            //return View(recar);
        }


        [HttpPost]
        public ActionResult Getid(string carno)
        {

            var carn = (from s in db.rentails
                        where s.carid == carno
                        select new
                        {
                            StartDate = s.sdate,
                            EndDate=s.edate,
                            Custid=s.custid,
                            CarNo=s.carid,
                            Fee=s.fee,
                            ElapsedDays=SqlFunctions.DateDiff("day",s.edate,DateTime.Now)
                        }).ToArray();

            return Json(carn, JsonRequestBehavior.AllowGet);

        }


    }
}