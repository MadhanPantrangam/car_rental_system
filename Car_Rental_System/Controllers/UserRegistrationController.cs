﻿using Car_Rental_System.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Car_Rental_System.Controllers
{
    public class UserRegistrationController : Controller
    {
        supercarEntities1 db = new supercarEntities1();
        // GET: UserRegistration
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SignUp()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignUp(tbl_User tbl_user_info)
        {
            if (ModelState.IsValid)
            {
                if (db.tbl_User.Any(x => x.Username == tbl_user_info.Username))
                {
                    ViewBag.Notification = "This Account has Already existed.";
                    return View();
                }
                else
                {
                    db.tbl_User.Add(tbl_user_info);
                    db.SaveChanges();
                    Session["UserId"] = tbl_user_info.UserId.ToString();
                    Session["Username"] = tbl_user_info.Username.ToString();
                    //return RedirectToAction("SignUp", "UserRegistration");
                }
            }
            return View(tbl_user_info);
        }
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(tbl_User tbl_user_info)
        {
            var checkLogin = db.tbl_User.Where(x => x.Username.Equals(tbl_user_info.Username) && x.Password.Equals(tbl_user_info.Password)).FirstOrDefault();
            if(checkLogin != null)
            {
               Session["isLogin"] = true;
                Session["UserId"] = tbl_user_info.UserId.ToString();
                Session["Username"] = tbl_user_info.Username.ToString();
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.alertMessage = "Wrong UserName And Password";
            }
            return View();
        }

        public ActionResult Logout()
        {
            Session["isLogin"] = false;
            Session.Clear();
            return RedirectToAction("Login", "UserRegistration");
        }
    }
}