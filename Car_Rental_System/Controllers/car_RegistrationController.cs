﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Car_Rental_System.Models;

namespace Car_Rental_System.Controllers
{
    public class car_RegistrationController : Controller
    {
        private supercarEntities1 db = new supercarEntities1();

        // GET: car_Registration
        public ActionResult Index()
        {
            return View(db.car_Registration.ToList());
        }

        // GET: car_Registration/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            car_Registration car_Registration = db.car_Registration.Find(id);
            if (car_Registration == null)
            {
                return HttpNotFound();
            }
            return View(car_Registration);
        }

        // GET: car_Registration/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: car_Registration/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,carno,make,model,available")] car_Registration car_Registration)
        {
            var isCarNos = db.car_Registration.FirstOrDefault(e => e.carno == car_Registration.carno);

          if(isCarNos==null)
          {
                if (ModelState.IsValid)
                {
                    db.car_Registration.Add(car_Registration);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

          }
          else
          {
                ViewBag.Message = "Already The Car Number is existed!";
          }

            return View(car_Registration);
        }

        // GET: car_Registration/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            car_Registration car_Registration = db.car_Registration.Find(id);
            if (car_Registration == null)
            {
                return HttpNotFound();
            }
            return View(car_Registration);
        }

        // POST: car_Registration/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,carno,make,model,available")] car_Registration car_Registration)
        {
            if (ModelState.IsValid)
            {
                db.Entry(car_Registration).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(car_Registration);
        }

        // GET: car_Registration/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            car_Registration car_Registration = db.car_Registration.Find(id);
            if (car_Registration == null)
            {
                return HttpNotFound();
            }
            return View(car_Registration);
        }

        // POST: car_Registration/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            car_Registration car_Registration = db.car_Registration.Find(id);
            db.car_Registration.Remove(car_Registration);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
