﻿using Car_Rental_System.Managers;
using Car_Rental_System.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Car_Rental_System.Controllers
{
    public class RentController : Controller
    {
        private IRentManager _rentManager;
        public RentController()
        {
            _rentManager = new RentManager();
        }
        supercarEntities1 db = new supercarEntities1();
        // GET: Rent
        public ActionResult Index()
        {
            var result = _rentManager.GetRentDetails();

            return View(result);
        }


        [HttpGet]
        public ActionResult GetCar()
        {
            var carList = _rentManager.GetCarRegistrations();

            return Json(carList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Getid(int id)
        {
            var customer = _rentManager.GetCustomerName(id);

            return Json(customer, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult Getavil(string carno)
        {
            var caravil = _rentManager.GetAvailableCar(carno);
            return Json(caravil, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Save(rentail rent)
        {
            if(ModelState.IsValid)
            {
                db.rentails.Add(rent);

                var car = _rentManager.checkExistRentDetail(rent.carid);
                if (car == null)
                {
                    return HttpNotFound("Car No not Valid");
                }
                car.available = "no";
                db.Entry(car).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(rent);

        }
    }
}