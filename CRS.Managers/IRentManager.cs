﻿using Car_Rental_System.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRS.Managers
{
    public interface IRentManager
    {
        List<RentailViewModel> GetRentDetails();
        List<car_Registration> GetCarRegistrations();
        string GetCustomerName();
        string GetAvailableCar();
        string SaveRentDetails();
    }
}
