﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Car_Rental_System.Models;

namespace CRS.Managers
{
    public class RentManager : IRentManager
    {
        supercarEntities1 db = new supercarEntities1();
        public string GetAvailableCar()
        {
            throw new NotImplementedException();
        }

        public List<car_Registration> GetCarRegistrations()
        {
            throw new NotImplementedException();
        }

        public string GetCustomerName()
        {
            throw new NotImplementedException();
        }

        public List<RentailViewModel> GetRentDetails()
        {
            var result = (from r in db.rentails
                          join c in db.car_Registration on r.carid equals c.carno
                          select new RentailViewModel
                          {
                              id = r.id,
                              carid = r.carid,
                              custid = r.custid,
                              fee = r.fee,
                              sdate = r.sdate,
                              edate = r.edate,
                              available = c.available

                          }).ToList();
            return result;
        }

        public string SaveRentDetails()
        {
            throw new NotImplementedException();
        }
    }
}
